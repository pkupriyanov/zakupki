import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'zakupki.settings')
os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')

app = Celery('zakupki')
app.config_from_object('django.conf:settings')


app.autodiscover_tasks()

app.conf.beat_schedule = {}