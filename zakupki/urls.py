from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from core import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('contracts/', views.ContractsView.as_view(), name='contracts'),
    path('contracts/<int:pk>/', views.ContractView.as_view(), name='contract'),
    path('companies/', views.CompaniesView.as_view(), name='companies'),
    path('companies/<int:pk>/', views.CompanyView.as_view(), name='company'),
    path('search/', views.SearchView.as_view(), name='search'),
    path('webhook/', csrf_exempt(views.WebhookView.as_view()), name='webhook'),
    path('admin/', admin.site.urls),
]
