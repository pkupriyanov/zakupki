from decimal import Decimal
from math import floor
import datetime

from django.db.models import Sum
from django.contrib.postgres.search import SearchVector

from core.models import Stage, Contract
from core.helpers.constants import STATUS_TYPES, CHOOSE_PROVIDER_TYPES, FINANCE_SOURCE_TYPES


def get_supplies(contract_purchases, stage_purchases):
    if not contract_purchases:
        return []
    contract = contract_purchases.first().contract
    supplies = [get_payed(contract)]
    supplies_dict = map_same_name_supplies(contract_purchases)

    for i, purchase in enumerate(supplies_dict.keys()):
        same_name_purchases = stage_purchases.filter(name=purchase)
        count = sum(p.quantity for p in same_name_purchases)
        if same_name_purchases:
            unit = same_name_purchases.first().unit or contract_purchases.filter(name=purchase).first().unit
        else:
            unit = contract_purchases.filter(name=purchase).first().unit
        stages = [get_context_stage(p) for p in same_name_purchases.order_by('stage__date')]
        supply = {
            "name": purchase,
            "supplied": count,
            "all": supplies_dict[purchase],
            "percent": floor((count / supplies_dict[purchase]) * 100),
            "unit": unit,
            "id": 'a' + str(i),
            "stages": stages
        }
        supplies.append(supply)
    return supplies


def map_same_name_supplies(contract_purchases):
    purchases = {}
    for purchase in contract_purchases:
        name = purchase.name
        if name in purchases.keys():
            purchases[name] += purchase.quantity
        else:
            purchases[name] = purchase.quantity
    return purchases


def get_context_stage(purchase):
    return {
        "document": purchase.stage.props_verification,
        "date": purchase.stage.date,
        "quantity": purchase.quantity
    }


def get_context_payed(stage):
    return {
        "document": stage.props_verification,
        "date": stage.date,
        "quantity": stage.payed
    }


def get_payed(contract):
    pay_stages = Stage.objects.filter(contract=contract).exclude(payed=Decimal(0.0))
    count = sum(p.payed for p in pay_stages)
    stages = [get_context_payed(s) for s in pay_stages]
    return {
        "name": "Оплата",
        "supplied": count,
        "all": contract.final_price,
        "percent": floor((count / contract.final_price) * 100),
        "unit": "рублей",
        "id": 'payed',
        "stages": stages
    }


def get_company_context(company):
    year = datetime.datetime.now().year
    year = datetime.date(year, 1, 1)
    return {
        "company": company,
        "contracts_provider": Contract.objects.filter(provider=company, status="done", end_date__gt=year).count(),
        "contracts_provider_sum": Contract.objects
            .filter(provider=company, status="done", end_date__gt=year)
            .aggregate(sum=Sum('final_price'))['sum'],
        "contracts_customer": Contract.objects.filter(customer=company, status="done", end_date__gt=year).count(),
        "contracts_customer_sum": Contract.objects
            .filter(customer=company, status="done", end_date__gt=year)
            .aggregate(sum=Sum('final_price'))['sum']
    }


def filter_contracts(queryset, cleaned_data):
    if cleaned_data.get('start_date_bot'):
        queryset = queryset.filter(start_date__gte=cleaned_data['start_date_bot'])
    if cleaned_data.get('start_date_top'):
        queryset = queryset.filter(start_date__lte=cleaned_data['start_date_top'])
    if cleaned_data.get('end_date_bot'):
        queryset = queryset.filter(end_date__gte=cleaned_data['end_date_bot'])
    if cleaned_data.get('end_date_top'):
        queryset = queryset.filter(end_date__lte=cleaned_data['end_date_top'])
    if cleaned_data.get('customer_inn'):
        queryset = queryset.filter(customer__inn=cleaned_data['customer_inn'])
    if cleaned_data.get('customer_name'):
        queryset = queryset.annotate(search=SearchVector('customer__name', 'customer__verbose_name')) \
            .filter(search=cleaned_data['customer_name'])
    if cleaned_data.get('provider_inn'):
        queryset = queryset.filter(provider__inn=cleaned_data['provider_inn'])
    if cleaned_data.get('provider_name'):
        queryset = queryset.annotate(search=SearchVector('provider__name', 'provider__verbose_name')) \
            .filter(search=cleaned_data['provider_name'])
    if cleaned_data.get('final_price_bot'):
        queryset = queryset.filter(final_price__gt=cleaned_data['final_price_bot'])
    if cleaned_data.get('final_price_top'):
        queryset = queryset.filter(final_price__lt=cleaned_data['final_price_top'])
    if cleaned_data.get('status'):
        queryset = queryset.filter(status=cleaned_data['status'])
    if cleaned_data.get('provider_type'):
        queryset = queryset.filter(choose_provider_type=cleaned_data['provider_type'])
    return queryset


def update_form_context(context, cleaned_data=None):
    context['statuses'] = ({'label': choice[1], 'value': choice[0]} for choice in STATUS_TYPES)
    context['provider_types'] = ({'label': choice[1], 'value': choice[0]} for choice in CHOOSE_PROVIDER_TYPES)
    context['finance_types'] = ({'label': choice[1], 'value': choice[0]} for choice in FINANCE_SOURCE_TYPES)
    if cleaned_data:
        context['form'] = cleaned_data
    return context


def filter_numbers(search):
    return ''.join(i for i in search if i.isdigit())
