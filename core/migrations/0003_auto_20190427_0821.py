# Generated by Django 2.2 on 2019-04-27 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20190418_0559'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contract',
            name='notice_number',
            field=models.CharField(max_length=60, unique=True),
        ),
        migrations.AlterField(
            model_name='contract',
            name='purchase_code',
            field=models.CharField(max_length=60, unique=True),
        ),
        migrations.AlterField(
            model_name='contract',
            name='reg_number',
            field=models.CharField(max_length=60, unique=True),
        ),
        migrations.AlterField(
            model_name='contract',
            name='schedule_number',
            field=models.CharField(max_length=60, null=True, unique=True),
        ),
    ]
