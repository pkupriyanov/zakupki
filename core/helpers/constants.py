DATE_FORMAT = '%d.%m.%Y'

STATUS_TYPES = (
    ('process', 'Исполнение'),
    ('done', 'Исполнение завершено'),
    ('stopped', 'Исполнение прекращено'),
    ('canceled_record', 'Аннулированная реестровая запись')
)

CHOOSE_PROVIDER_TYPES = (
    ('open_contest', 'Открытый конкурс'),
    ('limited_contest', 'Конкурс с ограниченным участием'),
    ('two-stage_contest', 'Двухэтапный конкурс'),
    ('electronic_contest', 'Электронный аукцион')
)

FINANCE_SOURCE_TYPES = (
    ('federal', 'Федеральный бюджет'),
    ('subject', 'Бюджет субъекта Российской Федерации'),
    ('local', 'Местный бюджет'),
    ('PF', 'Бюджет Пенсионного фонда Российской Федерации'),
    ('OMS', 'Бюджет Федерального фонда обязательного медицинского страхования'),
    ('FSS', 'Бюджет Фонда социального страхования Российской Федерации'),
)

CONTRACT_NAME_FIELDS = {
    'Реестровый номер контракта': 'reg_number',
    'Статус контракта': 'status',
    'Номер извещения об осуществлении закупки': 'notice_number',
    'Идентификационный код закупки (ИКЗ)': 'purchase_code',
    'Уникальный номер позиции плана-графика': 'schedule_number',
    'Способ определения поставщика (подрядчика, исполнителя)': 'choose_provider_type',
    'Дата подведения результатов определения поставщика (подрядчика, исполнителя)': 'choose_provider_date',
    'Дата размещения (по местному времени)': 'publish_date',
    'Реквизиты документа, подтверждающего основание заключения контракта': 'props_verification',
    'Номер контракта': 'local_number',
    'Дата заключения контракта': 'start_date',
    'Дата окончания исполнения контракта': 'end_date',
    'Цена контракта': 'final_price',
}

COMPANY_NAME_FIELDS = {
    'Полное наименование заказчика': 'name',
    'ИНН': 'inn',
    'Сокращенное наименование заказчика': 'verbose_name',
}

CONTRACT_PAGE_TABLE_NAMES = {
    'Общая информация': 'common_info',
    'Информация о заказчике': 'customer_info',
    'Общие данные': 'common_data',
    'Информация о поставщиках': 'provider_info',
}

COMMON_FIELDS = ('reg_number', 'status', 'start_date', 'end_date', 'final_price')
