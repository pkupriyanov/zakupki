import re
from datetime import datetime
from decimal import Decimal, InvalidOperation

from scrapy.selector.unified import SelectorList, Selector
from scrapy_djangoitem import DjangoItem
from django.db.models import Model

from core.helpers.constants import DATE_FORMAT


def get_link_values(rows: SelectorList, expression: str) -> list:
    return [get_text(row, expression) for row in rows if row.xpath(expression).extract()]


def get_values(rows: SelectorList, expression: str) -> list:
    return [get_text(row, expression) for row in rows]


def get_text(selector: Selector, expression: str) -> str:
    return selector.xpath(expression).extract()[0].strip()


def dict_from_choices(choices: tuple, reversed: bool = False) -> dict:
    data = choices
    if reversed:
        data = ((value, key) for key, value in choices)
    return dict(data)


def update_model(destination: Model, source: DjangoItem, commit: bool = True) -> Model:
    pk = destination.pk
    source_dict = dict(source)
    for (key, value) in source_dict.items():
        setattr(destination, key, value)
    setattr(destination, 'pk', pk)
    if commit:
        destination.save()

    return destination


def filter_rows(rows: SelectorList, expression: str, reverse_filter: bool = False) -> SelectorList:
    """
    remove rows without css expression
    :reverse_filter revert value (equal not)
    """
    return SelectorList(row for row in rows if reverse_filter ^ bool(row.css(expression)))


def clear_number(raw_num_str: str) -> str:
    """
    remove unparseble symbols
    """
    return raw_num_str.replace(' ', '').replace(',', '.').replace('\xa0', '')


def clear_text(raw_text: str) -> str:
    """
    remove double spaces, \n
    """
    return raw_text.replace('  ', '').replace('\n', '')


def get_clear_text(selector: Selector, expression: str) -> str:
    return clear_text(selector.xpath(expression).extract()[0].strip())


def get_clear_number(selector: Selector, expression: str) -> str:
    return clear_number(selector.xpath(expression).extract()[0].strip())


def get_date(text: str, date_format=DATE_FORMAT):
    return datetime.strptime(text, date_format)


def get_decimal(value):
    try:
        return Decimal(clear_number(value))
    except InvalidOperation:
        return Decimal(0)


def clear_webhook_reg_number(raw_reg_number):
    regex = re.compile('[0-9]+')
    return regex.search(raw_reg_number).group(0)
