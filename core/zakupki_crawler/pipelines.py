from copy import deepcopy

from scrapy import Spider
from scrapy_djangoitem import DjangoItem

from core.helpers.functions import update_model
from core.models import Company, Contract, Purchase, Stage


class CompaniesPipeline:
    """
    Save customer, provider from pipeline
    """

    def process_item(self, item: DjangoItem, spider: Spider):
        for field in ('customer', 'provider'):
            company = item[field]
            old_company, created = Company.objects.get_or_create(inn=company['inn'], defaults=company)
            if not created:
                update_model(old_company, company)
            else:
                old_company.save()
            item[field] = old_company
        return item


class ContractPipeline:

    def process_item(self, item: DjangoItem, spider: Spider):
        contract_data = deepcopy(item)
        contract_data.pop('stages')
        contract_data.pop('purchases')
        old_contact, created = Contract.objects.get_or_create(reg_number=item['reg_number'], defaults=contract_data)
        if not created:
            update_model(old_contact, contract_data)
        else:
            old_contact.save()
        return item


class PurchasesPipeline:

    def process_item(self, item: DjangoItem, spider: Spider):
        # created in previous pipeline
        contract = Contract.objects.get(reg_number=item['reg_number'])
        Purchase.objects.filter(contract=contract).delete()
        for p in item['purchases']:
            Purchase.objects.create(contract=contract, **p).save()
        return item


class StagesPipeline:

    def process_item(self, item: DjangoItem, spider: Spider):
        # created in previous pipeline
        contract = Contract.objects.get(reg_number=item['reg_number'])
        Stage.objects.filter(contract=contract).delete()
        for s in item['stages']:
            purchases = s.pop('purchases')
            stage = Stage.objects.create(contract=contract, **s)
            stage.save()
            for p in purchases:
                Purchase.objects.create(stage=stage, **p).save()
        return item
