BOT_NAME = 'zakupki_crawler'

SPIDER_MODULES = ['zakupki_crawler.spiders']
NEWSPIDER_MODULE = 'zakupki_crawler.spiders'

ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 32

DEFAULT_REQUEST_HEADERS = {
   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
   'Accept-Language': 'en',
}

ITEM_PIPELINES = {
    'zakupki_crawler.pipelines.CompaniesPipeline': 210,
    'zakupki_crawler.pipelines.ContractPipeline': 220,
    'zakupki_crawler.pipelines.PurchasesPipeline': 230,
    'zakupki_crawler.pipelines.StagesPipeline': 240,
}
