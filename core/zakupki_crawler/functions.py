from billiard import Process

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from core.zakupki_crawler.spiders.contract import ContractSpider


def process_deco(func):
    def wrapper(reg_number):
        p = Process(target=func, args=[reg_number])
        p.start()
        p.join()
        p.terminate()

    return wrapper


@process_deco
def parse(reg_number):
    process = CrawlerProcess(settings=get_project_settings())
    process.crawl(ContractSpider, reg_number=reg_number)
    process.start()
