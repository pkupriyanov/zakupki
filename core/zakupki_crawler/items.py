from scrapy import Field
from scrapy_djangoitem import DjangoItem

from core.models import Contract, Company, Stage, Purchase


class ContractItem(DjangoItem):
    django_model = Contract
    purchases = Field()
    stages = Field()


class CompanyItem(DjangoItem):
    django_model = Company


class StageItem(DjangoItem):
    django_model = Stage
    purchases = Field()


class PurchaseItem(DjangoItem):
    django_model = Purchase
