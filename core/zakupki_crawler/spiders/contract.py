from django.utils import timezone

import scrapy
from scrapy import signals
from scrapy.selector.unified import Selector, HtmlResponse

from twisted.python.failure import Failure

from core.helpers.constants import STATUS_TYPES, CHOOSE_PROVIDER_TYPES, FINANCE_SOURCE_TYPES, CONTRACT_NAME_FIELDS, \
    COMPANY_NAME_FIELDS, CONTRACT_PAGE_TABLE_NAMES
from core.helpers.functions import dict_from_choices, get_values, filter_rows, get_text, get_clear_number,\
    get_clear_text, get_date, get_decimal
from core.zakupki_crawler.items import ContractItem, CompanyItem, PurchaseItem, StageItem
from core.models import ParseLog

REVERSED_STATUSES = dict_from_choices(STATUS_TYPES, reversed=True)
REVERSED_PROVIDER_TYPES = dict_from_choices(CHOOSE_PROVIDER_TYPES, reversed=True)
REVERSED_FINANCE_SOURCES = dict_from_choices(FINANCE_SOURCE_TYPES, reversed=True)

# COMMON INFO
REG_NUMBER = 0
STATUS = 1
PURCHASE_CODE = 3
CHOOSE_PROVIDER_TYPE = 5
CHOOSE_PROVIDER_DATE = 6
PUBLISH_DATE = 7
PROPS_VERIFICATION = 8
# CUSTOMER
VERBOSE_NAME = 1
INN = 4
# COMMON DATA
LOCAL_NUMBER = 1
FINAL_PRICE = 3
START_DATE = 5
END_DATE = 6
FINANCE_SOURCE = 1
# PURCHASE
CODE = 1
UNIT = 2
COST = 3
QUANTITY = 5
# STAGE
STAGE_DATE = 1
STAGE_PROPS_VERIFICATION = 2
STAGE_PAYED = 3
STAGE_MISTAKE = 4


class LoggingSpider(scrapy.Spider):

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(LoggingSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_error, signal=signals.spider_error)
        return spider

    def spider_error(self, failure: Failure, response: HtmlResponse, spider: scrapy.Spider):
        log: ParseLog = response.meta.get('log')
        if not log:
            return
        log.error = str(failure)
        log.end_time = timezone.now()
        log.duration = log.end_time - log.start_time
        log.save()


class ContractSpider(LoggingSpider):
    name = 'contract'
    base_contract = 'http://zakupki.gov.ru/epz/contract/contractCard/common-info.html?reestrNumber={}'
    base_purchases = 'http://zakupki.gov.ru/epz/contract/contractCard/payment-info-and-target-of-order.html?reestrNumber={}'
    base_stages = 'http://zakupki.gov.ru/epz/contract/contractCard/process-info.html?reestrNumber={}'

    def start_requests(self):
        log = ParseLog(contract_reg_number=self.reg_number)
        log.save()
        meta = {'reg_number': self.reg_number, 'log': log}
        yield scrapy.Request(url=self.base_contract.format(self.reg_number), callback=self.parse_contract, meta=meta)

    def parse(self, response: HtmlResponse):
        pass

    def parse_contract(self, response: HtmlResponse):
        tables_block = response.css('div[class^="noticeTabBox"]')
        # common_info, customer_info, common_data, provider_info, *_ = tables
        headers = tables_block.xpath('h2//text()').extract()
        tables = tables_block.css('table[border="0"]')
        tables_dict = {}
        for i, header in enumerate(headers):
            key = CONTRACT_PAGE_TABLE_NAMES.get(header)
            if not key:
                continue
            tables_dict[key] = i
        contract = ContractItem()
        contract = parse_common_info(contract, tables[tables_dict['common_info']])
        contract = parse_customer_info(contract, tables[tables_dict['customer_info']])
        contract = parse_common_data(contract, tables[tables_dict['common_data']])
        contract = parse_provider_info(contract, tables[tables_dict['provider_info']])
        meta = response.meta
        reg_number = meta.get('reg_number')
        meta.update({'contract': contract})
        yield scrapy.Request(url=self.base_purchases.format(reg_number), callback=self.parse_purchases, meta=meta)

    def parse_purchases(self, response):
        contract: ContractItem = response.meta.get('contract')
        contract['purchases'] = []
        # TODO тестить на других, не лекарственных аукционах!
        tables = response.css('table[border="0"]:not([class^="drugTable"])')
        header, purchase_table, *_ = tables
        rows = purchase_table.css('tr:not([class^="drugInfo"])')
        rows = filter_rows(rows, 'td:not([class^="delimTr"])')[1:-1]
        rows = filter_rows(rows, 'td[class^="width"]', reverse_filter=True)
        rows = filter_rows(rows, 'td::attr(class)')
        for row in rows:
            contract['purchases'].append(parse_purchase(row))
        meta = response.meta
        meta.update({'contract': contract})
        yield scrapy.Request(url=self.base_stages.format(meta.get('reg_number')), callback=self.parse_stages, meta=meta)

    def parse_stages(self, response: HtmlResponse):
        contract: ContractItem = response.meta.get('contract')
        contract['stages'] = []
        stage = None
        stage_tables = response.css('table[class^="fulfillmentDocuments"]')
        if len(stage_tables):
            stage_table = stage_tables[0]
            rows = stage_table.xpath('tr')
            rows = filter_rows(rows, 'td:not([style^="display: none"])')[1:]
        else:
            rows = []
        while rows:
            row = rows[0]
            # Если first_td содержит дату, то создаем новый этап, если class=verticalMiddle - пропускаем, иначе
            # добавляем товар к этапу
            first_td = row.css('td')[0]
            # расширенная информация о закупке, пропускаем
            if first_td.css('td::attr(class)'):
                rows = rows[1:]
                continue
            # добавляем закупку к контракту
            if first_td.css('td::attr(colspan)'):
                if not stage:
                    rows = rows[1:]
                    continue
                stage['purchases'].append(parse_stage_purchase(row))
            # создаем новый контракт
            else:
                # сохраняем предыдущий
                if stage:
                    contract['stages'].append(stage)
                stage = StageItem()
                stage['date'] = get_date(get_text(row, f'td[{STAGE_DATE}]//text()'))
                stage['props_verification'] = get_clear_text(row, f'td[{STAGE_PROPS_VERIFICATION}]//text()')
                stage['payed'] = get_decimal(get_clear_number(row, f'td[{STAGE_PAYED}]//text()').split('(')[0])
                stage['mistake_information'] = get_text(row, f'td[{STAGE_MISTAKE}]//text()')
                stage['purchases'] = []
                # есть объекты закупки
                purchase_td = row.css('td')[4]
                if not purchase_td.css('td::attr(colspan)'):
                    stage['purchases'].append(parse_stage_purchase(row))
            rows = rows[1:]
        if stage:
            contract['stages'].append(stage)
        log: ParseLog = response.meta.get('log')
        log.end_time = timezone.now()
        log.duration = log.end_time - log.start_time
        log.success = True
        log.save()
        yield contract


def parse_common_info(contract: ContractItem, common_info: Selector) -> ContractItem:
    parse_functions = {
        'notice_number': lambda s: get_clear_text(s, 'td[2]//a//text()'),
        'schedule_number': lambda s: get_clear_text(s, 'td[2]//a//text()'),
        'reg_number': lambda s: get_clear_text(s, 'td[2]//text()').replace(' ', ''),
        'status': lambda s: REVERSED_STATUSES.get(get_clear_text(s, 'td[2]//text()')),
        'purchase_code': lambda s: get_clear_text(s, 'td[2]//text()'),
        'choose_provider_type': lambda s: REVERSED_PROVIDER_TYPES.get(get_clear_text(s, 'td[2]//text()')),
        'choose_provider_date': lambda s: get_date(get_clear_text(s, 'td[2]//text()')),
        'publish_date': lambda s: get_date(get_clear_text(s, 'td[2]//text()').split()[0]),
        'props_verification': lambda s: get_clear_text(s, 'td[2]//text()')
    }
    rows = common_info.css('tr')
    for row in rows:
        key = get_clear_text(row, 'td[1]//text()')
        if key not in CONTRACT_NAME_FIELDS.keys():
            continue
        parse_func = parse_functions[CONTRACT_NAME_FIELDS[key]]
        contract[CONTRACT_NAME_FIELDS[key]] = parse_func(row)
    return contract


def parse_customer_info(contract: ContractItem, customer_info: Selector) -> ContractItem:
    parse_functions = {
        'name': lambda s: get_clear_text(s, 'td[2]//a//text()'),
        'verbose_name': lambda s: get_clear_text(s, 'td[2]//text()'),
        'inn': lambda s: get_clear_text(s, 'td[2]//text()'),
    }
    customer = CompanyItem()
    rows = customer_info.css('tr')
    for row in rows:
        key = get_clear_text(row, 'td[1]//text()')
        if key not in COMPANY_NAME_FIELDS.keys():
            continue
        parse_func = parse_functions[COMPANY_NAME_FIELDS[key]]
        customer[COMPANY_NAME_FIELDS[key]] = parse_func(row)
    contract["customer"] = customer
    return contract


def parse_common_data(contract: ContractItem, common_data: Selector) -> ContractItem:
    parse_functions = {
        'local_number': lambda s: get_clear_text(s, 'td[2]//text()'),
        'start_date': lambda s: get_date(get_clear_text(s, 'td[2]//text()')),
        'end_date': lambda s: get_date(get_clear_text(s, 'td[2]//text()')),
        'final_price': lambda s: get_decimal(get_clear_text(s, 'td[2]//text()')),
    }
    rows = common_data.css('tr')
    for row in rows:
        key = get_clear_text(row, 'td[1]//text()')
        if key not in CONTRACT_NAME_FIELDS.keys():
            continue
        parse_func = parse_functions[CONTRACT_NAME_FIELDS[key]]
        contract[CONTRACT_NAME_FIELDS[key]] = parse_func(row)
    return contract


def parse_provider_info(contract: ContractItem, provider_info: Selector) -> ContractItem:
    provider = CompanyItem()
    company_data = provider_info.xpath('tr[3]//td[1]')
    company_name = company_data.xpath('text()')[1].extract().strip()
    # company_name = f"{name} ({verbose_name})"
    try:
        name, verbose_name = company_name.split('(')
    except ValueError:
        name, verbose_name = company_name, None
    provider['name'] = name.strip()
    if verbose_name:
        provider['verbose_name'] = verbose_name.replace(')', '').strip()
    # TODO: GET TEXT
    provider['inn'] = company_data.xpath('table//tr[2]//td[2]//text()').extract()[0].strip()
    provider['address'] = provider_info.xpath('tr[3]//td[4]//text()').extract()[0].strip()
    phone, email = provider_info.xpath('tr[3]//td[6]//text()').extract()
    provider['phone'] = phone.strip()
    provider['email'] = email.strip()
    contract['provider'] = provider
    return contract


def parse_purchase(row: Selector) -> PurchaseItem:
    item = PurchaseItem()
    values = get_values(row.xpath('td'), 'text()')
    item['name'] = get_clear_text(row, 'td[1]//div//text()')
    item['code'] = values[CODE]
    item['unit'] = values[UNIT]
    item['cost'] = get_decimal(values[COST])
    item['quantity'] = get_decimal(values[QUANTITY])
    return item


def parse_stage_purchase(row: Selector) -> PurchaseItem:
    count, name = 5, 6
    if row.css('td')[0].css('td::attr(colspan)'):
        count, name = 2, 3
    item = PurchaseItem()
    count_text = get_text(row, f'td[{count}]//text()')
    item['quantity'] = get_decimal(count_text.split()[0])
    item['unit'] = count_text.split('(')[1].replace(')', '')
    td = row.xpath(f'td[{name}]')
    name = get_clear_text(td, f'table//tr//td[{len(td.css("td")) - 1}]//text()')
    item['name'] = name[:name.rfind('(')].strip()
    item['code'] = name.split('(')[-1].split(')')[0]
    return item
