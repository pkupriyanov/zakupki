from zakupki.celery import app

from core.zakupki_crawler.functions import parse


@app.task
def run_parse(reg_number):
    parse(reg_number)
