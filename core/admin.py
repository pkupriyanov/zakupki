from django.contrib import admin
from .models import Company, Contract, Purchase, Stage, ParseLog

class StageInline(admin.TabularInline):
    model = Stage
    extra = 0

class PurchaseInline(admin.TabularInline):
    model = Purchase
    extra = 0


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'verbose_name', 'inn')
    search_fields = ('verbose_name', 'inn')


class ContractAdmin(admin.ModelAdmin):
    list_display = ('reg_number', 'status', 'final_price', 'customer', 'provider', 'start_date', 'end_date')
    search_fields = ('reg_number', 'status')
    inlines = [StageInline, PurchaseInline]


class StageAdmin(admin.ModelAdmin):
    inlines = [PurchaseInline]


admin.site.register(Company, CompanyAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Stage, StageAdmin)
admin.site.register(Purchase)
admin.site.register(ParseLog)
