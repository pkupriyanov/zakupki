from django import forms


class ContractsForm(forms.Form):
    start_date_bot = forms.DateField(required=False)
    start_date_top = forms.DateField(required=False)
    end_date_bot = forms.DateField(required=False)
    end_date_top = forms.DateField(required=False)
    customer_inn = forms.CharField(required=False)
    customer_name = forms.CharField(required=False)
    provider_inn = forms.CharField(required=False)
    provider_name = forms.CharField(required=False)
    final_price_bot = forms.DecimalField(required=False)
    final_price_top = forms.DecimalField(required=False)
    status = forms.CharField(required=False)
    provider_type = forms.CharField(required=False)


class CompaniesForm(forms.Form):
    inn = forms.CharField(required=False)
    name = forms.CharField(required=False)
