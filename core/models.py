from django.utils import timezone
from django.db import models

from core.helpers.constants import DATE_FORMAT, STATUS_TYPES, FINANCE_SOURCE_TYPES, CHOOSE_PROVIDER_TYPES
from core.helpers.functions import dict_from_choices


class Company(models.Model):
    class Meta:
        indexes = (models.Index(fields=('inn',)),)
        verbose_name = 'Организация'
        verbose_name_plural = 'Организации'

    name = models.CharField(max_length=500)
    inn = models.CharField(max_length=12)
    verbose_name = models.CharField(max_length=100, null=True, blank=True)
    address = models.CharField(null=True, blank=True, max_length=500)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(null=True, blank=True, max_length=40)

    def __str__(self):
        return f'{self.verbose_name or self.name}'


class Contract(models.Model):
    class Meta:
        ordering = ('publish_date',)
        indexes = (models.Index(fields=('reg_number',)),)
        verbose_name = 'Контракт'
        verbose_name_plural = 'Контракты'

    reg_number = models.CharField(unique=True, max_length=60)
    status = models.CharField(choices=STATUS_TYPES, max_length=15)
    notice_number = models.CharField(unique=True, max_length=60, null=True)
    purchase_code = models.CharField(unique=True, max_length=60, null=True)
    schedule_number = models.CharField(unique=True, max_length=60, null=True)
    choose_provider_type = models.CharField(choices=CHOOSE_PROVIDER_TYPES, max_length=18, null=True)
    choose_provider_date = models.DateField(null=True)
    publish_date = models.DateField(null=True)
    props_verification = models.CharField(max_length=1000, null=True)
    customer = models.ForeignKey(to=Company, null=True, on_delete=models.SET_NULL, related_name='customer_contract')
    provider = models.ForeignKey(to=Company, null=True, on_delete=models.SET_NULL, related_name='provider_contract')
    local_number = models.CharField(max_length=40, null=True)
    start_price = models.DecimalField(decimal_places=2, max_digits=15, null=True, blank=True)
    final_price = models.DecimalField(decimal_places=2, max_digits=15)
    start_date = models.DateField()
    end_date = models.DateField()
    finance_source = models.CharField(choices=FINANCE_SOURCE_TYPES, max_length=7, null=True, blank=True)

    def __str__(self):
        return f'Контракт {self.reg_number}, ' \
            f'Статус: {dict_from_choices(STATUS_TYPES)[self.status]}, Цена: {self.final_price}'


class Stage(models.Model):
    class Meta:
        ordering = ('-date',)
        verbose_name = 'Этап'
        verbose_name_plural = 'Этапы'

    date = models.DateField()
    props_verification = models.CharField(max_length=1000)
    payed = models.DecimalField(decimal_places=2, max_digits=15)
    mistake_information = models.CharField(max_length=1000)
    contract = models.ForeignKey(to=Contract, on_delete=models.CASCADE)

    def __str__(self):
        return f'Этап для контракта {self.contract.reg_number} от {self.date.strftime(DATE_FORMAT)}'


class Purchase(models.Model):
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    name = models.CharField(max_length=2000)
    code = models.CharField(max_length=100, null=True)
    unit = models.CharField(max_length=100)
    cost = models.DecimalField(decimal_places=2, max_digits=15, null=True)
    quantity = models.DecimalField(decimal_places=2, max_digits=15)
    contract = models.ForeignKey(to=Contract, null=True, default=None, on_delete=models.CASCADE)
    stage = models.ForeignKey(to=Stage, null=True, default=None, on_delete=models.CASCADE)

    def __str__(self):
        contract, stage = self.contract, self.stage
        if contract:
            substring = f'Позиция контракта {contract.reg_number}'
        elif stage:
            substring = f'Поставка для контракта {stage.contract.reg_number} от {stage.date.strftime(DATE_FORMAT)}'
        else:
            # так не должно быть!
            substring = ''
        return f'{substring} {self.name} {self.quantity} {self.unit}'


class ParseLog(models.Model):
    class Meta:
        verbose_name = 'Лог'
        verbose_name_plural = 'Логи'

    contract_reg_number = models.CharField(max_length=300)
    start_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(blank=True, null=True)
    duration = models.DurationField(blank=True, null=True)
    success = models.BooleanField(default=False)
    error = models.TextField(null=True, blank=True)

    def __str__(self):
        msg = f'{self.contract_reg_number}, продолжительность: {self.duration}'
        if self.error:
            msg += f'ERROR: {self.error}'
        return msg
