from django.core.management.base import BaseCommand

from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from core.zakupki_crawler.spiders.contract import ContractSpider


class Command(BaseCommand):

    def handle(self, *args, **options):
        process = CrawlerProcess(settings=get_project_settings())
        process.crawl(ContractSpider, reg_number='2753400428318000401')
        process.start(stop_after_crawl=False)
