from abc import ABC

from django.http.response import HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, DetailView, TemplateView, RedirectView, View
from django.contrib.postgres.search import SearchVector
from django.db.models import Sum, Q
from django.shortcuts import get_object_or_404, render

from core.models import Contract, Company, Stage, Purchase
from core.helpers.functions import clear_webhook_reg_number

from core.tasks import run_parse
from core.utils import get_supplies, get_company_context, filter_contracts, update_form_context, filter_numbers
from core.forms import ContractsForm, CompaniesForm


class HomeView(RedirectView):
    pattern_name = 'contracts'


class ContractsView(ListView):
    template_name = 'contracts.html'
    model = Contract
    paginate_by = 10

    def get_queryset(self):
        form = ContractsForm(self.request.GET)
        form.is_valid()
        cleaned_data = form.cleaned_data
        self._form_cleaned_data = cleaned_data
        queryset = Contract.objects.all()
        return filter_contracts(queryset, cleaned_data)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(self.__class__, self).get_context_data(object_list=object_list, **kwargs)
        cleaned_data = self.__dict__.get('_form_cleaned_data')
        context = update_form_context(context, cleaned_data)
        return context


class ContractView(DetailView):
    template_name = 'contract.html'
    model = Contract

    def get_context_data(self, **kwargs):
        context = {}
        contract = self.get_object()
        stages = Stage.objects.filter(contract=contract)
        stage_purchases = Purchase.objects.filter(stage__in=stages)
        contract_purchases = Purchase.objects.filter(contract=contract)
        context['contract'] = contract
        context['stages'] = ({
            'stage': stage,
            'purchases': stage_purchases.filter(stage=stage),
            'id': 'stage' + str(i)
        } for i, stage in enumerate(stages))
        context['purchases'] = contract_purchases
        context['supplies'] = get_supplies(contract_purchases, stage_purchases)
        return context


class CompaniesView(ListView):
    template_name = 'companies.html'
    model = Company
    paginate_by = 10

    def get_queryset(self):
        form = CompaniesForm(self.request.GET)
        form.is_valid()
        cleaned_data = form.cleaned_data
        self._form_cleaned_data = cleaned_data
        queryset = super(CompaniesView, self).get_queryset()
        if cleaned_data.get('inn'):
            queryset = queryset.filter(inn=cleaned_data['inn'])
        if cleaned_data.get('name'):
            queryset = queryset.annotate(search=SearchVector('name', 'verbose_name')) \
                .filter(search=cleaned_data['name'])
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CompaniesView, self).get_context_data()
        context["companies"] = [get_company_context(company) for company in context['company_list']]
        if self.__getattribute__('_form_cleaned_data'):
            context['form'] = self._form_cleaned_data
        return context


class CompanyView(ListView):
    template_name = 'company.html'
    model = Contract
    paginate_by = 10

    def get_object(self):
        return get_object_or_404(Company, pk=self.kwargs['pk'])

    def get_queryset(self):
        company = self.get_object()
        queryset = super(CompanyView, self).get_queryset()
        queryset = queryset.filter(Q(provider=company) | Q(customer=company))
        form = ContractsForm(self.request.GET)
        form.is_valid()
        cleaned_data = form.cleaned_data
        self._form_cleaned_data = cleaned_data
        return filter_contracts(queryset, cleaned_data)

    def get_context_data(self, *, object_list=None, **kwargs):
        company = self.get_object()
        context = super(CompanyView, self).get_context_data()
        context['company'] = get_company_context(company)
        cleaned_data = self.__dict__.get('_form_cleaned_data')
        context = update_form_context(context, cleaned_data)
        return context


class SearchView(View):

    def get(self, request):
        search = request.GET.get('search')
        search_num = filter_numbers(search)
        if search_num:
            try:
                contract = Contract.objects.get(
                    Q(reg_number=search_num) | Q(notice_number=search_num) | Q(purchase__code=search_num)
                )
                return HttpResponseRedirect(f'/contracts/{contract.pk}/')
            except Contract.DoesNotExist:
                pass
            try:
                company = Company.objects.get(inn=search_num)
                return HttpResponseRedirect(f'/companies/{company.pk}/')
            except Company.DoesNotExist:
                pass
        companies = Company.objects.annotate(search=SearchVector('name', 'verbose_name')).filter(search=search).exists()
        if companies:
            return HttpResponseRedirect(f'/companies/?name={search}')
        return render(request, 'error.html')


class WebhookView(View):

    def post(self, request):
        reg_number = clear_webhook_reg_number(request.POST.get('reg_number'))
        if reg_number:
            run_parse.delay(reg_number)
            return HttpResponse(status=200)
        return HttpResponse(status=400)
