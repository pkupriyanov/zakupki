import os
import sys

from django.conf import settings

sys.path.insert(0, settings.SCRAPY_MODULE_PATH)
os.environ['SCRAPY_SETTINGS_MODULE'] = settings.SCRAPY_SETTINGS_MODULE